/* Copyright © 2017 Tylor Allison */
using UnityEngine;
using System.Collections.Generic;

using TyMesh;
using TySimpleShape;

namespace TyTest {
    public class TestShapes: GenericTest {
        protected GameObjectPool goPool;
        protected MeshController meshController;
        protected void Setup() {
            var mcGO = new GameObject("MeshController");
            objectCache.Add(mcGO);
            meshController = mcGO.AddComponent<MeshController>();
            goPool = new GameObjectPool("test pool", new GameObjectFactory());
        }

        public override void Start() {
            Setup();
            // create meshdata
            var meshData = new MeshDataExpanding(96);

            var shapes = new SimpleShapes(meshData);
            var v1 = new Vector3(0,0,0);
            var v2 = new Vector3(5,0,0);
            var v3 = new Vector3(0,5,0);
            var v4 = new Vector3(0,0,5);
            var v5 = new Vector3(5,5,0);
            var v6 = new Vector3(5,0,5);
            var v7 = new Vector3(0,5,5);
            var v8 = new Vector3(5,5,5);
            shapes.DrawSphere(v1, Color.red);
            shapes.DrawSphere(v2, Color.red);
            shapes.DrawSphere(v3, Color.red);
            shapes.DrawSphere(v4, Color.red);
            shapes.DrawSphere(v5, Color.red);
            shapes.DrawSphere(v6, Color.red);
            shapes.DrawSphere(v7, Color.red);
            shapes.DrawSphere(v8, Color.red);
            shapes.DrawLine(v1, v2, Color.blue);
            shapes.DrawLine(v1, v3, Color.blue);
            shapes.DrawLine(v1, v4, Color.blue);
            shapes.DrawLine(v2, v5, Color.blue);
            shapes.DrawLine(v2, v6, Color.blue);
            shapes.DrawLine(v3, v5, Color.blue);
            shapes.DrawLine(v3, v7, Color.blue);
            shapes.DrawLine(v4, v6, Color.blue);
            shapes.DrawLine(v4, v7, Color.blue);
            shapes.DrawLine(v7, v8, Color.blue);
            shapes.DrawLine(v6, v8, Color.blue);
            shapes.DrawLine(v5, v8, Color.blue);

            shapes.DrawCircle(new Vector3(2.5f, 3.5f, 2.5f), Vector3.up, 2.5f, Color.green, 8);
            shapes.DrawCircle(new Vector3(2.5f, 1.5f, 2.5f), Vector3.up, 2.5f, Color.green, 16);
            shapes.DrawCircle(new Vector3(2.5f, .5f, 2.5f), Vector3.up, 2.5f, Color.green, 32);
            shapes.DrawDisk(new Vector3(2.5f, 0, 2.5f), Vector3.up, 2.5f, Color.green, 32);

            // create mesh converter
            var meshConverter = new MeshConverterColor();

            // create controller
            meshController.Setup(
                goPool,
                meshConverter
            );
            meshController.AssignMesh(Vector3.zero, meshData, "test");
        }

        public override void Stop() {
            base.Stop();
            goPool.Clear();
        }
    }
}
