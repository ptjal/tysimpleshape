﻿/* Copyright © 2017 Tylor Allison */

using UnityEngine;
using System;
using System.Text;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

using TyMesh;

namespace TySimpleShape {
    public static class IcosahedronShape {

        // ------------------------------------------------------
        // STATIC VARIABLES
        static float x0;
        static float x1;
        static float x2;
        static float y1;
        static float z0;
        static float z1;
        static float z2;
        static Vector3[] vertices;

        // ------------------------------------------------------
        // CONSTRUCTORS
        static IcosahedronShape() {
            float latitude = Mathf.Atan(0.5f);
            float longitude = Mathf.PI / 5;
            x0 = Mathf.Cos(latitude) * Mathf.Sin(longitude * 2);
            x1 = Mathf.Cos(latitude) * Mathf.Sin(longitude);
            x2 = 0.0f;
            y1 = Mathf.Sin(latitude);
            z0 = Mathf.Cos(latitude);
            z1 = Mathf.Cos(latitude) * Mathf.Cos(longitude);
            z2 = Mathf.Cos(latitude) * Mathf.Cos(longitude * 2);
            vertices = new Vector3[] {
                new Vector3(0.0f, 1.0f, 0.0f),
                new Vector3( x2,  y1,  z0),
                new Vector3( x2, -y1, -z0),
                new Vector3(0.0f, -1.0f, 0.0f),
                new Vector3(-x1,  y1, -z1),
                new Vector3( x1,  y1, -z1),
                new Vector3(-x1, -y1,  z1),
                new Vector3( x1, -y1,  z1),
                new Vector3(-x0,  y1,  z2),
                new Vector3(-x0, -y1, -z2),
                new Vector3( x0,  y1,  z2),
                new Vector3( x0, -y1, -z2)
            };

        }

        public static void AddToMesh(
            IMeshData meshData,
            uint materialId,
            Vector3 origin,
            float radius
        ) {
            meshData.AddFace(materialId, vertices[8]*radius + origin,
                                         vertices[1]*radius + origin,
                                         vertices[0]*radius + origin);
            meshData.AddFace(materialId, vertices[4]*radius + origin,
                                         vertices[8]*radius + origin,
                                         vertices[0]*radius + origin);
            meshData.AddFace(materialId, vertices[5]*radius + origin,
                                         vertices[4]*radius + origin,
                                         vertices[0]*radius + origin);
            meshData.AddFace(materialId, vertices[10]*radius + origin,
                                         vertices[5]*radius + origin,
                                         vertices[0]*radius + origin);
            meshData.AddFace(materialId, vertices[1]*radius + origin,
                                         vertices[10]*radius + origin,
                                         vertices[0]*radius + origin);
            meshData.AddFace(materialId, vertices[7]*radius + origin,
                                         vertices[10]*radius + origin,
                                         vertices[1]*radius + origin);
            meshData.AddFace(materialId, vertices[6]*radius + origin,
                                         vertices[7]*radius + origin,
                                         vertices[1]*radius + origin);
            meshData.AddFace(materialId, vertices[8]*radius + origin,
                                         vertices[6]*radius + origin,
                                         vertices[1]*radius + origin);
            meshData.AddFace(materialId, vertices[11]*radius + origin,
                                         vertices[3]*radius + origin,
                                         vertices[2]*radius + origin);
            meshData.AddFace(materialId, vertices[5]*radius + origin,
                                         vertices[11]*radius + origin,
                                         vertices[2]*radius + origin);
            meshData.AddFace(materialId, vertices[4]*radius + origin,
                                         vertices[5]*radius + origin,
                                         vertices[2]*radius + origin);
            meshData.AddFace(materialId, vertices[9]*radius + origin,
                                         vertices[4]*radius + origin,
                                         vertices[2]*radius + origin);
            meshData.AddFace(materialId, vertices[3]*radius + origin,
                                         vertices[9]*radius + origin,
                                         vertices[2]*radius + origin);
            meshData.AddFace(materialId, vertices[6]*radius + origin,
                                         vertices[9]*radius + origin,
                                         vertices[3]*radius + origin);
            meshData.AddFace(materialId, vertices[7]*radius + origin,
                                         vertices[6]*radius + origin,
                                         vertices[3]*radius + origin);
            meshData.AddFace(materialId, vertices[11]*radius + origin,
                                         vertices[7]*radius + origin,
                                         vertices[3]*radius + origin);
            meshData.AddFace(materialId, vertices[9]*radius + origin,
                                         vertices[8]*radius + origin,
                                         vertices[4]*radius + origin);
            meshData.AddFace(materialId, vertices[10]*radius + origin,
                                         vertices[11]*radius + origin,
                                         vertices[5]*radius + origin);
            meshData.AddFace(materialId, vertices[8]*radius + origin,
                                         vertices[9]*radius + origin,
                                         vertices[6]*radius + origin);
            meshData.AddFace(materialId, vertices[11]*radius + origin,
                                         vertices[10]*radius + origin,
                                         vertices[7]*radius + origin);
        }

    }
}
