﻿/* Copyright © 2017 Tylor Allison */

using UnityEngine;
using System;
using System.Text;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

using TyMesh;

namespace TySimpleShape {
    public static class CylinderShape {

        public static void AddToMesh(
            IMeshData meshData,
            uint materialIndex,
            float radius,
            Vector3 endA,
            Vector3 endB
        ) {
            AddToMesh(
                meshData,
                materialIndex,
                endA+((endB-endA)/2.0f),
                radius,
                (endB-endA).magnitude,
                Quaternion.FromToRotation(Vector3.up, (endB-endA).normalized)
            );
        }

        public static void AddToMesh(
            IMeshData meshData,
            uint materialIndex,
            Vector3 origin,
            float radius,
            float length
        ) {
            AddToMesh(
                meshData,
                materialIndex,
                origin,
                radius,
                length,
                Quaternion.identity);
        }

        public static void AddToMesh(
            IMeshData meshData,
            uint materialIndex,
            Vector3 origin,
            float radius,
            float length,
            Quaternion rotation
        ) {
            // FIXME: precompute
            var NumberOfSides = 8;
            var textureStepU = 1f / NumberOfSides;
            var angInc = 2f * Mathf.PI * textureStepU;
            var ang = 0f;

            // Add ring vertices
            Vector3 lastVertex = Vector3.zero;
            var halfLength = length/2;
            var bottomCenter = new Vector3(0,-halfLength,0);
            var topCenter = new Vector3(0,halfLength,0);
            var lengthV = new Vector3(0,length,0);
            for (var n = 0; n <= NumberOfSides; n++, ang += angInc) {
                var vertex = new Vector3(radius * Mathf.Cos(ang),
                                         -halfLength,
                                         radius * Mathf.Sin(ang));
                if (n>0) {
                    // side of cylinder
                    meshData.AddFace(materialIndex,
                        origin+rotation*(lastVertex),
                        origin+rotation*(lastVertex+lengthV),
                        origin+rotation*(vertex+lengthV),
                        origin+rotation*(vertex)
                    );
                    // top of cylinder
                    meshData.AddFace(materialIndex,
                        origin+rotation*(lastVertex+lengthV),
                        origin+rotation*(topCenter),
                        origin+rotation*(vertex+lengthV)
                    );
                    // bottom of cylinder
                    meshData.AddFace(materialIndex,
                        origin+rotation*(vertex),
                        origin+rotation*(bottomCenter),
                        origin+rotation*(lastVertex)
                    );
                }
                lastVertex = vertex;
            }
        }
    }
}
