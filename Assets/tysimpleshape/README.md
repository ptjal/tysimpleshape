# tysimpleshape - A Unity package providing simple line and dot drawing

## Design

Two different types of classes are provided.  

* Shape classes - cylinder and icosahedron
* **SimpleShapes** class - provides methods for line/dot drawing using TyMesh primitives

A simple way to create meshes w/ lines and dots... wow!

## Install
Add the following to Packages/manifest.json
```
    "com.ptjal.tymesh": "https://bitbucket.org/ptjal/tymesh.git#0.1.0",
    "com.ptjal.tysimpleshape": "https://bitbucket.org/ptjal/tysimpleshape.git#0.1.0",
```

## Usage

This is dependent on the **tymesh** module for creating and rendering dynamic meshes.

Here's a quick example:

    // create controlling game object instantiate mesh controller
    var mcGO = new GameObject("MeshController");
    var meshController = mcGO.AddComponent<MeshController>();

    // create mesh data storage
    var meshData = new MeshDataExpanding(96);

    // Add shapes to the mesh
    var meshData = new MeshDataExpanding(96);
    var shapes = new SimpleShapes(meshData);
    var v1 = new Vector3(0,0,0);
    var v2 = new Vector3(5,0,0);
    shapes.DrawSphere(v1, Color.red);
    shapes.DrawSphere(v2, Color.red);
    shapes.DrawLine(v1, v2, Color.blue);

    // create mesh converter
    var meshConverter = new MeshConverterColor();

    // create controller
    var meshController = CreateMeshController();
    meshController.meshConverter = meshConverter;
    meshController.AssignMesh(Vector3.zero, meshData, "test");

